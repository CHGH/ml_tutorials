import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns


def separate(info = 'NO'):
    print('\n============== ' + info + '\n')


GRAPHS_PATH = './graphs/'
TELECOM_CHURN_PATH = '../data/telecom_churn.csv'

df = pd.read_csv(TELECOM_CHURN_PATH)

separate()
# g1 = sns.countplot(x='international plan', hue='churn', data=df)

print(pd.crosstab(df['churn'], df['customer service calls'], margins=True))
# g2 = sns.countplot(x='customer service calls', hue='churn', data=df)

df['many_service_calls'] = (df['customer service calls'] > 3).astype('int')
print(pd.crosstab(df['many_service_calls'], df['churn'], margins=True))
g3 = sns.countplot(x='many_service_calls', hue='churn', data=df)


separate()
print(pd.crosstab(df['many_service_calls'] & df['international plan'], df['churn']))




plt.show()


