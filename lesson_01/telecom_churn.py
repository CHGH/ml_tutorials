import numpy as np
import pandas as pd
import warnings
# warnings.filterwarnings('ignore')

def separate(info = 'NO'):
    print('\n============== ' + info + '\n')

TELECOM_CHURN_PATH = '../data/telecom_churn.csv'

df = pd.read_csv(TELECOM_CHURN_PATH)

print(df.head())

print(df.shape)

print(df.columns)

print(df.info())

# change column type
# df['churn'] = df['churn'].astype('int64')

separate()
print(df.describe(include=['object', 'bool']))

separate()
print(df['churn'].value_counts())
# calculace fractions
print(df['churn'].value_counts(normalize=True))

separate()

# sorting
print(df.sort_values(by=['churn', 'total day charge'], ascending=[True, False]).head())

separate()

# indexing and retrieving data
print(df['churn'].mean())
separate()
# what are the average values of numerical features for churned users?
print(df[df['churn'] == 1].mean())
separate()
# how much time (on average) do churned/non-churned users spend on phone during daytime
print(df[df['churn'] == 1]['total day minutes'].mean())
print(df[df['churn'] == 0]['total day minutes'].mean())
separate()
# what is the maximum length of international calls among loyal users (churn == 0),
# who do not have an international plan?
print(df[(df['churn'] == 0) & (df['international plan'] == 'no')]['total intl minutes'].max())

separate()
# index by name
print(df.loc[0:5, 'state':'area code'])
separate()
# index by number
print(df.iloc[0:5, 0:3])

separate()
# first line of the data frame
print(df[-1:])

separate()
# first line of the data frame
print(df[:1])

separate()
# applying functions to each column
print(df.apply(np.max))

separate()
print(df[df['state'].apply(lambda state: state[0] == 'W')].head())

separate()
# replace values in a column
d = {'no': False, 'yes': True}
df['international plan'] = df['international plan'].map(d)
print(df.head())

separate()
df = df.replace({'voice mail plan': d})
print(df.head())

# grouping
# in general: df.groupby(by = grouping_columns)[columns_to_show].function()
separate()
columns_to_show = ['total day minutes', 'total eve minutes', 'total night minutes']
print(df.groupby('churn')[columns_to_show].describe(percentiles=[]))

separate()
print(df.groupby('churn')[columns_to_show].agg([np.mean, np.std, np.min, np.max]))

# summary tables (contingency table - таблица сопряженности)
separate()
print(pd.crosstab(df['churn'], df['international plan']))
separate()
print(pd.crosstab(df['churn'], df['voice mail plan']))
separate("INFO")
print(pd.crosstab(df['churn'], df['voice mail plan'], normalize=True))

separate('average number of day, evening, and night calls by area code')
print(df.pivot_table(['total day calls', 'total eve calls', 'total night calls'], ['area code'], aggfunc='mean'))

separate('DataFrame transformations')
total_calls = df['total day calls'] + df['total eve calls'] \
    + df['total night calls'] + df['total intl calls']
df.insert(loc=len(df.columns), column='total calls', value=total_calls)
# loc parameter is the number of columns after which to insert the Series object
# we set it to len(df.columns) to paste it at the very end of the dataframe
print(df.head())
# It is possible to add a column more easily without creating an intermediate Series instance:
# df['Total charge'] = df['Total day charge'] + df['Total eve charge'] + \
#                      df['Total night charge'] + df['Total intl charge']
# df.head()

separate()
# get rid of just created columns
df.drop(['total night charge', 'total night calls'], axis=1, inplace=True)
# and here’s how you can delete rows
print(df.drop([1, 2]).head())

separate("predict attempt")
print(pd.crosstab(df['churn'], df['international plan'], margins=True))
